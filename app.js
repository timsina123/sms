	// cosntants definition
	const express = require("express");
	const bodyParser=require("express");
	const ejs = require("ejs");
	const Nexmo=require("nexmo");
	const socketio=require("socket.io");

	// init nexmo

	const nexmo=new Nexmo({

		apiKey:'6511e122',
		apiSecret:'0d37b7805b2134c2'
	},
	{debug:true});

	// init app

	const app= express();

	// trmplate engine setup

	app.set('view engine','html');
	app.engine('html',ejs.renderFile);

	app.use(express.static(__dirname+'/public'));

	// body parser middleware
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended:true}));

	// index rout
	app.get('/',(req,res)=>{
		res.render('index');
	})


	//catch form submit

	app.post('/',(req,res)=>{
		const number=req.body.number;
		const text=req.body.text;
	// emit message
	nexmo.message.sendSms(
		'NEXMO',number,text,{type:'unicode'},(err,responseData)=>{
			if(err){
				console.log(err);
			}
			else{
				console.dir(responseData);

			// get data from response

			const data={

				id:responseData.messages[0]['message-id'],
				number:responseData.messages[0]['to']
			}

			// emit dT to client

			io.emit('smsStatus',data);
		}

	}
	)

	})


	//port definition and app listen
	const port=3000;
	const server=app.listen(port,()=> console.log('server started on port '+port));

	// connect to socket.io
	const io=socketio(server);
	io.on('connection',(socket)=>{
		console.log('connected');

		io.on('disconnect',()=>{
			console.log('disconnected');
		})
	})